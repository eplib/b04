<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="B04671">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>Mr. Penn's advice in the choice of Parliament-men, in his Englands great interest in the choice of this new Parliament ; dedicated to all her free-holders and electors.</title>
    <author>Penn, William, 1644-1718.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription B04671 of text R181610 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing P1249A). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">B04671</idno>
    <idno type="STC">Wing P1249A</idno>
    <idno type="STC">ESTC R181610</idno>
    <idno type="EEBO-CITATION">53981693</idno>
    <idno type="OCLC">ocm 53981693</idno>
    <idno type="VID">180297</idno>
    <idno type="PROQUESTGOID">2240862850</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. B04671)</note>
    <note>Transcribed from: (Early English Books Online ; image set 180297)</note>
    <note>Images scanned from microfilm: (Early English Books, 1641-1700 ; 2824:47)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>Mr. Penn's advice in the choice of Parliament-men, in his Englands great interest in the choice of this new Parliament ; dedicated to all her free-holders and electors.</title>
      <author>Penn, William, 1644-1718.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>s.n.],</publisher>
      <pubPlace>[London :</pubPlace>
      <date>published this fourth of December, 1688.</date>
     </publicationStmt>
     <notesStmt>
      <note>Caption title.</note>
      <note>Place of publication suggested by Wing.</note>
      <note>Excerpted from the author's "Englands great interest in the choice of this new Parliament", originally published in 1679.</note>
      <note>"The abovesaid being not unseasonable at this present conjecture, it is thought meet to have it thus published this fourth of December, 1688"--colophon.</note>
      <note>Reproduction of the original in the National Library of Scotland.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>England and Wales. -- Parliament -- Elections -- Early works to 1800.</term>
     <term>Great Britain -- Politics and government -- 1660-1688 -- Early works to 1800.</term>
     <term>Broadsides -- England -- 17th century.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>Mr. Penn's advice in the choice of Parliament-men, in his Englands great interest in the choice of this new Parliament; dedicated to all her free-hold</ep:title>
    <ep:author>Penn, William, </ep:author>
    <ep:publicationYear>1688</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>282</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2008-04</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2008-06</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2008-07</date>
    <label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change>
    <date>2008-07</date>
    <label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2008-09</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="B04671-t">
  <body xml:id="B04671-e0">
   <div type="text" xml:id="B04671-e10">
    <pb facs="tcp:180297:1" xml:id="B04671-001-a"/>
    <head xml:id="B04671-e20">
     <hi xml:id="B04671-e30">
      <w lemma="mr." pos="ab" xml:id="B04671-001-a-0010">Mr.</w>
      <w lemma="penn" pos="nng1" xml:id="B04671-001-a-0020">Penn's</w>
     </hi>
     <w lemma="advice" pos="n1" xml:id="B04671-001-a-0030">ADVICE</w>
     <w lemma="in" pos="acp" xml:id="B04671-001-a-0040">In</w>
     <w lemma="the" pos="d" xml:id="B04671-001-a-0050">the</w>
     <w lemma="choice" pos="n1" xml:id="B04671-001-a-0060">Choice</w>
     <w lemma="of" pos="acp" xml:id="B04671-001-a-0070">of</w>
     <w lemma="parliament-man" pos="n2" xml:id="B04671-001-a-0080">Parliament-Men</w>
     <pc xml:id="B04671-001-a-0090">,</pc>
     <w lemma="in" pos="acp" xml:id="B04671-001-a-0100">IN</w>
     <w lemma="his" pos="po" xml:id="B04671-001-a-0110">HIS</w>
     <hi xml:id="B04671-e40">
      <w lemma="England" pos="nng1" reg="England's" xml:id="B04671-001-a-0120">Englands</w>
      <w lemma="great" pos="j" xml:id="B04671-001-a-0130">great</w>
      <w lemma="interest" pos="n1" xml:id="B04671-001-a-0140">Interest</w>
      <w lemma="in" pos="acp" xml:id="B04671-001-a-0150">in</w>
      <w lemma="the" pos="d" xml:id="B04671-001-a-0160">the</w>
      <w lemma="choice" pos="n1" xml:id="B04671-001-a-0170">Choice</w>
      <w lemma="of" pos="acp" xml:id="B04671-001-a-0180">of</w>
      <w lemma="this" pos="d" xml:id="B04671-001-a-0190">this</w>
      <w lemma="new" pos="j" xml:id="B04671-001-a-0200">New</w>
      <w lemma="parliament" pos="n1" xml:id="B04671-001-a-0210">Parliament</w>
      <pc xml:id="B04671-001-a-0220">;</pc>
     </hi>
    </head>
    <head type="sub" xml:id="B04671-e50">
     <w lemma="dedicate" pos="vvn" xml:id="B04671-001-a-0230">Dedicated</w>
     <w lemma="to" pos="acp" xml:id="B04671-001-a-0240">to</w>
     <w lemma="all" pos="d" xml:id="B04671-001-a-0250">all</w>
     <w lemma="her" pos="po" xml:id="B04671-001-a-0260">her</w>
     <w lemma="freeholders" pos="ng1" reg="Freeholders'" xml:id="B04671-001-a-0270">Free-holders</w>
     <w lemma="and" pos="cc" xml:id="B04671-001-a-0280">and</w>
     <w lemma="elector" pos="n2" xml:id="B04671-001-a-0290">Electors</w>
     <pc unit="sentence" xml:id="B04671-001-a-0300">.</pc>
    </head>
    <p xml:id="B04671-e60">
     <w lemma="pray" pos="vvb" xml:id="B04671-001-a-0310">PRay</w>
     <w lemma="see" pos="vvb" xml:id="B04671-001-a-0320">see</w>
     <w lemma="that" pos="cs" xml:id="B04671-001-a-0330">that</w>
     <w lemma="you" pos="pn" xml:id="B04671-001-a-0340">you</w>
     <w lemma="choose" pos="vvb" reg="choose" xml:id="B04671-001-a-0350">chuse</w>
     <w lemma="sincere" pos="j" xml:id="B04671-001-a-0360">sincere</w>
     <w lemma="protestant" pos="nn2" xml:id="B04671-001-a-0370">Protestants</w>
     <pc xml:id="B04671-001-a-0380">:</pc>
     <w lemma="Men" pos="nn1" xml:id="B04671-001-a-0390">Men</w>
     <w lemma="that" pos="cs" xml:id="B04671-001-a-0400">that</w>
     <w lemma="do" pos="vvbx" xml:id="B04671-001-a-0410">don't</w>
     <w lemma="play" pos="vvi" xml:id="B04671-001-a-0420">play</w>
     <w lemma="the" pos="d" xml:id="B04671-001-a-0430">the</w>
     <w lemma="Protestant" pos="nn1" xml:id="B04671-001-a-0440">Protestant</w>
     <w lemma="in" pos="acp" xml:id="B04671-001-a-0450">in</w>
     <w lemma="design" pos="n1" xml:id="B04671-001-a-0460">Design</w>
     <pc xml:id="B04671-001-a-0470">,</pc>
     <w lemma="and" pos="cc" xml:id="B04671-001-a-0480">and</w>
     <w lemma="be" pos="vvb" xml:id="B04671-001-a-0490">are</w>
     <w lemma="indeed" pos="av" xml:id="B04671-001-a-0500">indeed</w>
     <w lemma="disguise" pos="vvn" reg="disguised" xml:id="B04671-001-a-0510">disguis'd</w>
     <w lemma="papist" pos="nn2" xml:id="B04671-001-a-0520">Papists</w>
     <pc xml:id="B04671-001-a-0530">,</pc>
     <w lemma="ready" pos="j" xml:id="B04671-001-a-0540">ready</w>
     <w lemma="to" pos="prt" xml:id="B04671-001-a-0550">to</w>
     <w lemma="pull" pos="vvi" xml:id="B04671-001-a-0560">pull</w>
     <w lemma="off" pos="acp" xml:id="B04671-001-a-0570">off</w>
     <w lemma="their" pos="po" xml:id="B04671-001-a-0580">their</w>
     <w lemma="mask" pos="n2" xml:id="B04671-001-a-0590">Masks</w>
     <w lemma="when" pos="crq" xml:id="B04671-001-a-0600">when</w>
     <w lemma="time" pos="n1" xml:id="B04671-001-a-0610">time</w>
     <w lemma="serve" pos="vvz" xml:id="B04671-001-a-0620">serves</w>
     <pc xml:id="B04671-001-a-0630">:</pc>
     <w lemma="you" pos="pn" xml:id="B04671-001-a-0640">You</w>
     <w lemma="will" pos="vmb" xml:id="B04671-001-a-0650">will</w>
     <w lemma="know" pos="vvi" xml:id="B04671-001-a-0660">know</w>
     <w lemma="such" pos="d" xml:id="B04671-001-a-0670">such</w>
     <w lemma="by" pos="acp" xml:id="B04671-001-a-0680">by</w>
     <w lemma="their" pos="po" xml:id="B04671-001-a-0690">their</w>
     <w lemma="laugh" pos="vvg" xml:id="B04671-001-a-0700">Laughing</w>
     <w lemma="at" pos="acp" xml:id="B04671-001-a-0710">at</w>
     <w lemma="the" pos="d" xml:id="B04671-001-a-0720">the</w>
     <hi xml:id="B04671-e70">
      <w lemma="plot" pos="n1" xml:id="B04671-001-a-0730">Plot</w>
      <pc xml:id="B04671-001-a-0740">,</pc>
     </hi>
     <w lemma="disgrace" pos="vvg" xml:id="B04671-001-a-0750">disgracing</w>
     <w lemma="the" pos="d" xml:id="B04671-001-a-0760">the</w>
     <w lemma="evidence" pos="n1" xml:id="B04671-001-a-0770">Evidence</w>
     <pc xml:id="B04671-001-a-0780">,</pc>
     <w lemma="admire" pos="vvg" xml:id="B04671-001-a-0790">admiring</w>
     <w lemma="the" pos="d" xml:id="B04671-001-a-0800">the</w>
     <w lemma="traitor" pos="ng1" reg="Traitor's" rend="hi" xml:id="B04671-001-a-0810">Traytors</w>
     <w lemma="constancy" pos="n1" xml:id="B04671-001-a-0820">Constancy</w>
     <w lemma="that" pos="cs" xml:id="B04671-001-a-0830">that</w>
     <w lemma="be" pos="vvd" xml:id="B04671-001-a-0840">were</w>
     <w lemma="force" pos="vvn" reg="forced" xml:id="B04671-001-a-0850">forc'd</w>
     <w lemma="to" pos="acp" xml:id="B04671-001-a-0860">to</w>
     <w lemma="it" pos="pn" xml:id="B04671-001-a-0870">it</w>
     <pc xml:id="B04671-001-a-0880">,</pc>
     <w lemma="or" pos="cc" xml:id="B04671-001-a-0890">or</w>
     <w lemma="their" pos="po" xml:id="B04671-001-a-0900">their</w>
     <w lemma="religion" pos="n1" xml:id="B04671-001-a-0910">Religion</w>
     <w lemma="and" pos="cc" xml:id="B04671-001-a-0920">and</w>
     <w lemma="party" pos="n1" xml:id="B04671-001-a-0930">Party</w>
     <w lemma="be" pos="vvd" xml:id="B04671-001-a-0940">were</w>
     <w lemma="go" pos="vvn" xml:id="B04671-001-a-0950">gone</w>
     <w lemma="beyond" pos="acp" xml:id="B04671-001-a-0960">beyond</w>
     <w lemma="a" pos="d" xml:id="B04671-001-a-0970">an</w>
     <w lemma="excuse" pos="n1" xml:id="B04671-001-a-0980">Excuse</w>
     <pc xml:id="B04671-001-a-0990">,</pc>
     <w lemma="or" pos="cc" xml:id="B04671-001-a-1000">or</w>
     <w lemma="a" pos="d" xml:id="B04671-001-a-1010">an</w>
     <w lemma="equivocation" pos="n1" xml:id="B04671-001-a-1020">Equivocation</w>
     <pc unit="sentence" xml:id="B04671-001-a-1030">.</pc>
     <w lemma="the" pos="d" xml:id="B04671-001-a-1040">The</w>
     <w lemma="contrary" pos="j" xml:id="B04671-001-a-1050">contrary</w>
     <w lemma="be" pos="vvb" xml:id="B04671-001-a-1060">are</w>
     <w lemma="man" pos="n2" xml:id="B04671-001-a-1070">Men</w>
     <w lemma="that" pos="cs" xml:id="B04671-001-a-1080">that</w>
     <w lemma="thank" pos="vvb" xml:id="B04671-001-a-1090">thank</w>
     <w lemma="GOD" pos="nn1" xml:id="B04671-001-a-1100">GOD</w>
     <w lemma="for" pos="acp" xml:id="B04671-001-a-1110">for</w>
     <w lemma="this" pos="d" xml:id="B04671-001-a-1120">this</w>
     <w lemma="discovery" pos="n1" xml:id="B04671-001-a-1130">Discovery</w>
     <pc xml:id="B04671-001-a-1140">,</pc>
     <w lemma="and" pos="cc" xml:id="B04671-001-a-1150">and</w>
     <w lemma="in" pos="acp" xml:id="B04671-001-a-1160">in</w>
     <w lemma="their" pos="po" xml:id="B04671-001-a-1170">their</w>
     <w lemma="conversation" pos="n1" xml:id="B04671-001-a-1180">Conversation</w>
     <w lemma="zealous" pos="av-j" xml:id="B04671-001-a-1190">zealously</w>
     <w lemma="direct" pos="vvi" xml:id="B04671-001-a-1200">direct</w>
     <w lemma="themselves" pos="pr" xml:id="B04671-001-a-1210">themselves</w>
     <w lemma="in" pos="acp" xml:id="B04671-001-a-1220">in</w>
     <w lemma="a" pos="d" xml:id="B04671-001-a-1230">an</w>
     <w lemma="opposition" pos="n1" xml:id="B04671-001-a-1240">Opposition</w>
     <w lemma="to" pos="acp" xml:id="B04671-001-a-1250">to</w>
     <w lemma="the" pos="d" xml:id="B04671-001-a-1260">the</w>
     <w lemma="papal" pos="j" rend="hi" xml:id="B04671-001-a-1270">Papal</w>
     <w lemma="interest" pos="n1" xml:id="B04671-001-a-1280">Interest</w>
     <pc xml:id="B04671-001-a-1290">,</pc>
     <w lemma="which" pos="crq" xml:id="B04671-001-a-1300">which</w>
     <w lemma="indeed" pos="av" xml:id="B04671-001-a-1310">indeed</w>
     <w lemma="be" pos="vvz" xml:id="B04671-001-a-1320">is</w>
     <w lemma="a" pos="d" xml:id="B04671-001-a-1330">a</w>
     <w lemma="combination" pos="n1" xml:id="B04671-001-a-1340">Combination</w>
     <w lemma="against" pos="acp" xml:id="B04671-001-a-1350">against</w>
     <w lemma="good" pos="j" xml:id="B04671-001-a-1360">good</w>
     <w lemma="sense" pos="n1" xml:id="B04671-001-a-1370">Sense</w>
     <pc xml:id="B04671-001-a-1380">,</pc>
     <w lemma="reason" pos="n1" xml:id="B04671-001-a-1390">Reason</w>
     <w lemma="and" pos="cc" xml:id="B04671-001-a-1400">and</w>
     <w lemma="conscience" pos="n1" xml:id="B04671-001-a-1410">Conscience</w>
     <pc xml:id="B04671-001-a-1420">,</pc>
     <w lemma="and" pos="cc" xml:id="B04671-001-a-1430">and</w>
     <w lemma="to" pos="prt" xml:id="B04671-001-a-1440">to</w>
     <w lemma="introduce" pos="vvi" xml:id="B04671-001-a-1450">introduce</w>
     <w lemma="a" pos="d" xml:id="B04671-001-a-1460">a</w>
     <w lemma="blind" pos="j" xml:id="B04671-001-a-1470">blind</w>
     <w lemma="obedience" pos="n1" xml:id="B04671-001-a-1480">Obedience</w>
     <w lemma="without" pos="acp" xml:id="B04671-001-a-1490">without</w>
     <pc join="right" xml:id="B04671-001-a-1500">(</pc>
     <w lemma="if" pos="cs" xml:id="B04671-001-a-1510">if</w>
     <w lemma="not" pos="xx" xml:id="B04671-001-a-1520">not</w>
     <w lemma="against" pos="acp" xml:id="B04671-001-a-1530">against</w>
     <pc xml:id="B04671-001-a-1540">)</pc>
     <w lemma="conviction" pos="n1" xml:id="B04671-001-a-1550">Conviction</w>
     <pc xml:id="B04671-001-a-1560">;</pc>
     <w lemma="and" pos="cc" xml:id="B04671-001-a-1570">and</w>
     <w lemma="that" pos="cs" xml:id="B04671-001-a-1580">that</w>
     <w lemma="principle" pos="n1" xml:id="B04671-001-a-1590">Principle</w>
     <w lemma="which" pos="crq" xml:id="B04671-001-a-1600">which</w>
     <w lemma="introduce" pos="vvz" xml:id="B04671-001-a-1610">introduces</w>
     <w lemma="implicit" pos="j" xml:id="B04671-001-a-1620">implicit</w>
     <w lemma="faith" pos="n1" xml:id="B04671-001-a-1630">Faith</w>
     <w lemma="and" pos="cc" xml:id="B04671-001-a-1640">and</w>
     <w lemma="blind" pos="j" xml:id="B04671-001-a-1650">blind</w>
     <w lemma="obedience" pos="n1" xml:id="B04671-001-a-1660">Obedience</w>
     <w lemma="in" pos="acp" xml:id="B04671-001-a-1670">in</w>
     <w lemma="religion" pos="n1" xml:id="B04671-001-a-1680">Religion</w>
     <pc xml:id="B04671-001-a-1690">,</pc>
     <hi xml:id="B04671-e100">
      <w lemma="will" pos="vmb" xml:id="B04671-001-a-1700">will</w>
      <w lemma="also" pos="av" xml:id="B04671-001-a-1710">also</w>
     </hi>
     <w lemma="introduce" pos="vvi" xml:id="B04671-001-a-1720">introduce</w>
     <w lemma="implicit" pos="j" xml:id="B04671-001-a-1730">implicit</w>
     <w lemma="faith" pos="n1" xml:id="B04671-001-a-1740">Faith</w>
     <w lemma="and" pos="cc" xml:id="B04671-001-a-1750">and</w>
     <w lemma="blind" pos="j" xml:id="B04671-001-a-1760">blind</w>
     <w lemma="obedience" pos="n1" xml:id="B04671-001-a-1770">Obedience</w>
     <w lemma="in" pos="acp" xml:id="B04671-001-a-1780">in</w>
     <w lemma="government" pos="n1" xml:id="B04671-001-a-1790">Government</w>
     <pc xml:id="B04671-001-a-1800">;</pc>
     <w lemma="so" pos="av" xml:id="B04671-001-a-1810">so</w>
     <w lemma="that" pos="cs" xml:id="B04671-001-a-1820">that</w>
     <w lemma="it" pos="pn" xml:id="B04671-001-a-1830">it</w>
     <w lemma="be" pos="vvz" xml:id="B04671-001-a-1840">is</w>
     <w lemma="no" pos="avx-d" xml:id="B04671-001-a-1850">no</w>
     <w lemma="more" pos="avc-d" xml:id="B04671-001-a-1860">more</w>
     <w lemma="the" pos="d" xml:id="B04671-001-a-1870">the</w>
     <w lemma="law" pos="n1" xml:id="B04671-001-a-1880">Law</w>
     <w lemma="in" pos="acp" xml:id="B04671-001-a-1890">in</w>
     <w lemma="the" pos="d" xml:id="B04671-001-a-1900">the</w>
     <w lemma="one" pos="pi" xml:id="B04671-001-a-1910">one</w>
     <w lemma="than" pos="cs" xml:id="B04671-001-a-1920">than</w>
     <w lemma="in" pos="acp" xml:id="B04671-001-a-1930">in</w>
     <w lemma="the" pos="d" xml:id="B04671-001-a-1940">the</w>
     <w lemma="other" pos="pi-d" xml:id="B04671-001-a-1950">other</w>
     <pc xml:id="B04671-001-a-1960">,</pc>
     <w lemma="but" pos="acp" xml:id="B04671-001-a-1970">but</w>
     <w lemma="the" pos="d" xml:id="B04671-001-a-1980">the</w>
     <w lemma="will" pos="n1" xml:id="B04671-001-a-1990">Will</w>
     <w lemma="and" pos="cc" xml:id="B04671-001-a-2000">and</w>
     <w lemma="power" pos="n1" xml:id="B04671-001-a-2010">power</w>
     <w lemma="of" pos="acp" xml:id="B04671-001-a-2020">of</w>
     <w lemma="the" pos="d" xml:id="B04671-001-a-2030">the</w>
     <w lemma="superior" pos="j" xml:id="B04671-001-a-2040">Superior</w>
     <pc xml:id="B04671-001-a-2050">,</pc>
     <w lemma="that" pos="cs" xml:id="B04671-001-a-2060">that</w>
     <w lemma="shall" pos="vmb" xml:id="B04671-001-a-2070">shall</w>
     <w lemma="be" pos="vvi" xml:id="B04671-001-a-2080">be</w>
     <w lemma="the" pos="d" xml:id="B04671-001-a-2090">the</w>
     <w lemma="rule" pos="n1" xml:id="B04671-001-a-2100">Rule</w>
     <w lemma="and" pos="cc" xml:id="B04671-001-a-2110">and</w>
     <w lemma="bond" pos="n1" xml:id="B04671-001-a-2120">Bond</w>
     <w lemma="of" pos="acp" xml:id="B04671-001-a-2130">of</w>
     <w lemma="our" pos="po" xml:id="B04671-001-a-2140">our</w>
     <w lemma="subjection" pos="n1" xml:id="B04671-001-a-2150">Subjection</w>
     <pc xml:id="B04671-001-a-2160">:</pc>
     <w lemma="this" pos="d" xml:id="B04671-001-a-2170">This</w>
     <w lemma="be" pos="vvz" xml:id="B04671-001-a-2180">is</w>
     <w lemma="that" pos="d" xml:id="B04671-001-a-2190">that</w>
     <w lemma="fatal" pos="j" rend="hi" xml:id="B04671-001-a-2200">Fatal</w>
     <w lemma="mischief" pos="n1" xml:id="B04671-001-a-2210">Mischief</w>
     <w lemma="propery" pos="n1" rend="hi" xml:id="B04671-001-a-2220">Propery</w>
     <w lemma="bring" pos="vvz" xml:id="B04671-001-a-2230">brings</w>
     <w lemma="with" pos="acp" xml:id="B04671-001-a-2240">with</w>
     <w lemma="it" pos="pn" xml:id="B04671-001-a-2250">it</w>
     <w lemma="to" pos="acp" xml:id="B04671-001-a-2260">to</w>
     <w lemma="civil" pos="j" xml:id="B04671-001-a-2270">civil</w>
     <w lemma="society" pos="n1" xml:id="B04671-001-a-2280">Society</w>
     <pc xml:id="B04671-001-a-2290">,</pc>
     <w lemma="and" pos="cc" xml:id="B04671-001-a-2300">and</w>
     <w lemma="for" pos="acp" xml:id="B04671-001-a-2310">for</w>
     <w lemma="which" pos="crq" xml:id="B04671-001-a-2320">which</w>
     <w lemma="such" pos="d" xml:id="B04671-001-a-2330">such</w>
     <w lemma="society" pos="n2" xml:id="B04671-001-a-2340">Societies</w>
     <w lemma="ought" pos="vmd" xml:id="B04671-001-a-2350">ought</w>
     <w lemma="to" pos="prt" xml:id="B04671-001-a-2360">to</w>
     <w lemma="be" pos="vvi" xml:id="B04671-001-a-2370">be</w>
     <w lemma="aware" pos="j" xml:id="B04671-001-a-2380">aware</w>
     <w lemma="of" pos="acp" xml:id="B04671-001-a-2390">of</w>
     <w lemma="it" pos="pn" xml:id="B04671-001-a-2400">it</w>
     <pc xml:id="B04671-001-a-2410">,</pc>
     <w lemma="and" pos="cc" xml:id="B04671-001-a-2420">and</w>
     <w lemma="all" pos="d" rend="hi" xml:id="B04671-001-a-2430">All</w>
     <w lemma="those" pos="d" xml:id="B04671-001-a-2440">those</w>
     <w lemma="that" pos="cs" xml:id="B04671-001-a-2450">that</w>
     <w lemma="be" pos="vvb" xml:id="B04671-001-a-2460">are</w>
     <w lemma="friend" pos="n2" xml:id="B04671-001-a-2470">Friends</w>
     <w lemma="to" pos="acp" xml:id="B04671-001-a-2480">to</w>
     <w lemma="it" pos="pn" xml:id="B04671-001-a-2490">it</w>
     <pc unit="sentence" xml:id="B04671-001-a-2500">.</pc>
     <pc join="right" xml:id="B04671-001-a-2510">[</pc>
     <w lemma="pag." pos="ab" rend="hi" xml:id="B04671-001-a-2520">Pag.</w>
     <w lemma="4." pos="crd" xml:id="B04671-001-a-2530">4.</w>
     <pc unit="sentence" xml:id="B04671-001-a-2540">]</pc>
    </p>
   </div>
  </body>
  <back xml:id="B04671-e150">
   <div type="colophon" xml:id="B04671-e160">
    <p xml:id="B04671-e170">
     <w lemma="the" pos="d" xml:id="B04671-001-a-2550">The</w>
     <w lemma="abovesaid" pos="j" xml:id="B04671-001-a-2560">abovesaid</w>
     <w lemma="being" pos="n1" xml:id="B04671-001-a-2570">being</w>
     <w lemma="not" pos="xx" xml:id="B04671-001-a-2580">not</w>
     <w lemma="unseasonable" pos="j" xml:id="B04671-001-a-2590">unseasonable</w>
     <w lemma="at" pos="acp" xml:id="B04671-001-a-2600">at</w>
     <w lemma="this" pos="d" xml:id="B04671-001-a-2610">this</w>
     <w lemma="present" pos="j" xml:id="B04671-001-a-2620">present</w>
     <w lemma="conjuncture" pos="n1" xml:id="B04671-001-a-2630">Conjuncture</w>
     <pc xml:id="B04671-001-a-2640">,</pc>
     <w lemma="it" pos="pn" xml:id="B04671-001-a-2650">it</w>
     <w lemma="be" pos="vvz" xml:id="B04671-001-a-2660">is</w>
     <w lemma="think" pos="vvn" xml:id="B04671-001-a-2670">thought</w>
     <w lemma="meet" pos="j" xml:id="B04671-001-a-2680">meet</w>
     <w lemma="to" pos="prt" xml:id="B04671-001-a-2690">to</w>
     <w lemma="have" pos="vvi" xml:id="B04671-001-a-2700">have</w>
     <w lemma="it" pos="pn" xml:id="B04671-001-a-2710">it</w>
     <w lemma="thus" pos="av" xml:id="B04671-001-a-2720">thus</w>
     <w lemma="publish" pos="j-vn" xml:id="B04671-001-a-2730">Published</w>
     <date xml:id="B04671-e180">
      <w lemma="this" pos="d" xml:id="B04671-001-a-2740">this</w>
      <w lemma="four" pos="ord" xml:id="B04671-001-a-2750">Fourth</w>
      <w lemma="of" pos="acp" xml:id="B04671-001-a-2760">of</w>
      <hi xml:id="B04671-e190">
       <w lemma="December" pos="nn1" xml:id="B04671-001-a-2770">December</w>
       <pc xml:id="B04671-001-a-2780">,</pc>
      </hi>
      <w lemma="1688." pos="crd" xml:id="B04671-001-a-2790">1688.</w>
      <pc unit="sentence" xml:id="B04671-001-a-2800"/>
     </date>
    </p>
    <trailer xml:id="B04671-e200">
     <w lemma="n/a" pos="fla" xml:id="B04671-001-a-2810">FINIS</w>
     <pc unit="sentence" xml:id="B04671-001-a-2820">.</pc>
    </trailer>
   </div>
  </back>
 </text>
</TEI>
